module Potherca
  module Dashing
    # Container for various authorization tokens
    class AuthenticationTokens
      attr_reader :tokens
      # @param ["#{each}"] container
      def initialize(container)
        @tokens = {}
        # @TODO: Check if ENV['DASHING_AUTH_TOKEN'] is actually set
        @tokens[:DASHING_AUTH_TOKEN] = container['DASHING_AUTH_TOKEN']
      end
    end
  end
end
